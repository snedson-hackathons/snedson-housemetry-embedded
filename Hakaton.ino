#include <Wire.h>
#include <VEML6075.h>
#include <BH1750.h>
#include <GY21.h>
#include "MCP3221.h"
#include <SDS011.h>
#include <HardwareSerial.h>
#include <WiFi.h> // библиотека Wi-Fi
#include <PubSubClient.h>
 const char* ssid = "IT_Academy_Hack"; // логин
 const char* password = "hack_2024"; // пароль
IPAddress server(94, 241, 140, 148); // Указать IP-адрес MQTT-сервера

//===========================Переменные===========================//
VEML6075 veml6075;
BH1750 lightMeter;
GY21 sensor;
MCP3221 mcp3221(0x4C);
HardwareSerial port(2);
SDS011 my_sds;
float p10, p25;
int err;
//===========================Переменные===========================//

//=========================Функции Wifi===========================//
String translateEncryptionType(wifi_auth_mode_t encryptionType) {
 // функция определения спецификации шифрования точки доступа
  switch (encryptionType) {
  case (WIFI_AUTH_OPEN):
  return "Open";
  case (WIFI_AUTH_WEP):
  return "WEP";
  case (WIFI_AUTH_WPA_PSK):
  return "WPA_PSK";
  case (WIFI_AUTH_WPA2_PSK):
  return "WPA2_PSK";
  case (WIFI_AUTH_WPA_WPA2_PSK):
  return "WPA_WPA2_PSK";
  case (WIFI_AUTH_WPA2_ENTERPRISE):
  return "WPA2_ENTERPRISE";
 }
 }

void scanNetworks() { // сканирование доступных Wi-Fi соединений
  int numberOfNetworks = WiFi.scanNetworks();
  Serial.print("Number of networks found: ");
  Serial.println(numberOfNetworks);
  for (int i = 0; i < numberOfNetworks; i++) {
  Serial.print("Network name: "); // имя сети
  Serial.println(WiFi.SSID(i));
  Serial.print("Signal strength: "); // уровень сигнала
  Serial.println(WiFi.RSSI(i));
  Serial.print("MAC address: "); // МAC-адрес
  Serial.println(WiFi.BSSIDstr(i));

  Serial.print("Encryption type: "); // тип шифрования
  String encryptionTypeDescription = translateEncryptionType(
  WiFi.encryptionType(i));
  Serial.println(encryptionTypeDescription);
  Serial.println("-----------------------");
 }
 }

 void connectToNetwork() {
  // функция подключения ESP32 к выбранной Wi-Fi точке
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
  delay(1000);
  Serial.println("Establishing connection to WiFi..");
  }
  Serial.println("Connected to network");
 }
//=========================Функции Wifi===========================//

//============================MQTT================================//
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

WiFiClient espClient;
PubSubClient client(espClient);

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("Housemetry")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("ac293413-0863-4c02-908d-22319d2ef4fa", "Chinases");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
//============================MQTT================================//

void setup() {
  // Инициализация UART и последовательного порта 
  my_sds.begin(&port, 16, 17);
  Serial.begin(115200);
  //-------------------------WIFI---------------------------//
  scanNetworks(); // сканирование сетей 
  connectToNetwork(); // подключение к выбранной Wi-Fi 
  Serial.print("MAC devise: ");
  Serial.println(WiFi.macAddress()); // Определение MAC-адреса устройства 
  Serial.print("IP local: ");
  Serial.println(WiFi.localIP()); // Определение локального IP-адреса 
  //-------------------------WIFI---------------------------//

  //-------------------------MQTT---------------------------//
  client.setServer(server, 1883);
  client.setCallback(callback);
  //-------------------------MQTT---------------------------//

  //---------------------------УФ---------------------------//
  // Инициализация датчика 
  Wire.begin(21, 22);
  Wire.setClock(10000L);
  if (!veml6075.begin())
    Serial.println("VEML6075 not found!");
  //---------------------------УФ---------------------------//

  //------------------------Освещение-----------------------//
  lightMeter.begin();
  //------------------------Освещение-----------------------//
}

void loop() {
  //-------------------------MQTT---------------------------//
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  //-------------------------MQTT---------------------------//
  // Измерение
  veml6075.poll();
  //-------Считывание УФ-------//
  float uva = veml6075.getUVA();
  float uvb = veml6075.getUVB();
  float uv_index = veml6075.getUVIndex();
  float lux = lightMeter.readLightLevel();  // считывание освещенности
  float temp = sensor.GY21_Temperature();
  float hum = sensor.GY21_Humidity();
  float adc0 = mcp3221.getVoltage();
  err = my_sds.read(&p25, &p10);
  String otpravka = "Chinases";
  // Вывод измеренных значений в терминал
  Serial.print("UVA = " + String(uva, 1) + " ");
  Serial.println("нм");
  Serial.print("UVB = " + String(uvb, 1) + " ");
  Serial.println("нм");
  //Serial.println("UV INDEX = " + String(uv_index, 1) + "   ");

  Serial.print("Освещенность: ");
  Serial.print(lux);
  Serial.println(" Люкс");

  Serial.print("Температура: ");
  Serial.print(temp );
  Serial.println("°C");
  Serial.print("Влажность: ");
  Serial.print(hum);
  Serial.println("%");

  Serial.println("Уровень шума (звука) = " + String(adc0, 1));

  if (!err) {
    Serial.print("P2.5: " + String(p25));
    Serial.println(" мкг/m^3");
    Serial.print("P10:  " + String(p10));
    Serial.println(" мкг/m^3");
  }
  otpravka = String(("{ ") + String("\"PM25\":") + " " +  String(p25) + ',' + String("\"P10\":") + " " + String(p10) + ',' + String("\"Noise\":") + " " +  String(adc0, 1) + ',' + String("\"Uva\":") + " " +  String(uva, 1) + ',' + String("\"Uvb\":") + " " +  String(uvb, 1) + ',' + String("\"Illumination\":") + " " +  String(lux) + ',' + String("\"Humidity\":") + " " +  String(hum) + ',' + String("\"Temp\":") + " " +  String(temp) + " " + String("}"));
  char arr[otpravka.length()+1];
  otpravka.toCharArray(arr, otpravka.length()+1);
  Serial.println(arr);
  client.publish("ac293413-0863-4c02-908d-22319d2ef4fa", arr);
  Serial.println();
  delay(60000);
}
